-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2023 at 06:01 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dans_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_user`
--

CREATE TABLE `mst_user` (
  `id` int(11) NOT NULL,
  `counter_wrong_password` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `is_deleted` varchar(255) DEFAULT NULL,
  `last_change_password` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `password` longtext DEFAULT NULL,
  `reason_blocked` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `token` longtext DEFAULT NULL,
  `token_expired` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `user_uuid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `verify_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mst_user`
--

INSERT INTO `mst_user` (`id`, `counter_wrong_password`, `created_date`, `email`, `first_name`, `full_name`, `ip_address`, `is_deleted`, `last_change_password`, `last_login`, `last_name`, `last_update`, `password`, `reason_blocked`, `role_id`, `status`, `token`, `token_expired`, `user_agent`, `user_uuid`, `username`, `verify_code`) VALUES
(1, NULL, '2023-02-19 22:53:16', 'arfinsyadziy@gmail.com', 'Arfin', 'Syadziy', NULL, 'N', NULL, '2023-02-19 23:18:48', 'Syadziy', NULL, 'b17825cc8a7c68b9100376a31f61aeaffae84284cae52334dabafe1bcef049b528e7cf5aed729e8a5d3cc3ffeaa6230d3d6f33a702f84a758e721df1a8032351', NULL, 1, 1, '36d58fc06108dbea0322b5f39b9df3238a948924197159d43f761fd66bb285c8193142208c3d984735b4a8f2e3a69590d59fb654ec3871dbe7e13930471a4e24', '2023-02-20 00', NULL, 'df34f39b-fd93-4872-8ab3-7796ab3a9bf8', 'syadziy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_user_id_seq`
--

CREATE TABLE `mst_user_id_seq` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mst_user_id_seq`
--

INSERT INTO `mst_user_id_seq` (`next_val`) VALUES
(2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_user`
--
ALTER TABLE `mst_user`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
