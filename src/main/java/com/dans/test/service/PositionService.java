/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.service;

import com.dans.test.dto.PositionResponseDTO;
import com.dans.test.general.ExcelService;
import com.dans.test.response.ResponseDTO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author arfinsyadziy
 */
@Service
public class PositionService {

    @Autowired
    private ExcelService exService;

    private int rowCount;
    private RestTemplate restTemplate = new RestTemplate();

    public ResponseEntity<?> get(ResponseDTO response) {
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            ResponseEntity<Object> rspAPI = restTemplate.exchange(
                    "http://dev3.dansmultipro.co.id/api/recruitment/positions.json",
                    HttpMethod.GET,
                    new HttpEntity<>(httpHeaders),
                    Object.class);

            response = response.success();
            response.setData(rspAPI.getBody());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(response.internalServerError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> getById(ResponseDTO response, String id) {
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            ResponseEntity<Object> rspAPI = restTemplate.exchange(
                    "http://dev3.dansmultipro.co.id/api/recruitment/positions/{ID}",
                    HttpMethod.GET,
                    new HttpEntity<>(httpHeaders),
                    Object.class,
                    id);

            response = response.success();
            response.setData(rspAPI.getBody());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(response.internalServerError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void download(HttpServletResponse response) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("User");
        XSSFCellStyle background = workbook.createCellStyle();
        exService.header(sheet, workbook);
        rowCount = 3;

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        ResponseEntity<PositionResponseDTO[]> rspAPI = restTemplate.exchange(
                "http://dev3.dansmultipro.co.id/api/recruitment/positions.json",
                HttpMethod.GET,
                new HttpEntity<>(httpHeaders),
                PositionResponseDTO[].class);
        
        List<PositionResponseDTO> listPosition = new ArrayList<PositionResponseDTO>(Arrays.asList(rspAPI.getBody()));
        listPosition.forEach((dto) -> {
            // creating row 
            background.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            background.setBorderTop(HSSFCellStyle.BORDER_THIN);
            background.setBorderRight(HSSFCellStyle.BORDER_THIN);
            background.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            Row row = sheet.createRow(++rowCount);

            Cell cell0 = row.createCell(0);
            cell0.setCellValue(dto.getId());
            cell0.setCellStyle(background);

            Cell cell1 = row.createCell(1);
            cell1.setCellValue(dto.getType());
            cell1.setCellStyle(background);

            Cell cell2 = row.createCell(2);
            cell2.setCellValue(dto.getUrl());
            cell2.setCellStyle(background);

            Cell cell3 = row.createCell(3);
            cell3.setCellValue(dto.getCreatedAt());
            cell3.setCellStyle(background);

            Cell cell4 = row.createCell(4);
            cell4.setCellValue(dto.getCompany());
            cell4.setCellStyle(background);

            Cell cell5 = row.createCell(5);
            cell5.setCellValue(dto.getCompanyUrl());
            cell5.setCellStyle(background);

            Cell cell6 = row.createCell(6);
            cell6.setCellValue(dto.getLocation());
            cell6.setCellStyle(background);

            Cell cell7 = row.createCell(7);
            cell7.setCellValue(dto.getTitle());
            cell7.setCellStyle(background);

            Cell cell8 = row.createCell(8);
            cell8.setCellValue(dto.getDescription());
            cell8.setCellStyle(background);

            Cell cell9 = row.createCell(9);
            cell9.setCellValue(dto.getHowToApply());
            cell9.setCellStyle(background);

            Cell cell10 = row.createCell(10);
            cell10.setCellValue(dto.getCompanyUrl());
            cell10.setCellStyle(background);
        });

        exService.generateExcel(response, workbook, "Position.csv");
    }

}
