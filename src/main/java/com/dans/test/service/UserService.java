/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.service;

import com.dans.test.assembler.UserAssembler;
import com.dans.test.dto.UserRequestDTO;
import com.dans.test.entity.UserEntity;
import com.dans.test.enums.StatusEnum;
import com.dans.test.general.GeneralFunction;
import com.dans.test.repository.UserRepository;
import com.dans.test.response.ResponseDTO;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author arfinsyadziy
 */
@Service
public class UserService {

    @Autowired
    private GeneralFunction gFunction;

    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<?> create(ResponseDTO response, UserRequestDTO request) throws NoSuchAlgorithmException {
        try {
            if (userRepository.findUserByUsername(request.getUsername().toLowerCase()) != null || userRepository.findUserByEmail(request.getEmail().toLowerCase()) != null) {
                return new ResponseEntity<>(response.alreadyExist(request.getUsername() + " or " + request.getEmail()), HttpStatus.OK);
            }

            UserEntity user = new UserEntity();
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setFullName(request.getFirstName() + request.getLastName() == null || request.getLastName().isEmpty() ? "" : request.getLastName());
            user.setEmail(request.getEmail().toLowerCase());
            user.setUsername(request.getUsername().toLowerCase());
            user.setPassword(gFunction.generatePassword(request.getPassword(), "dans_test"));
            user.setUserUuid(UUID.randomUUID().toString());
            user.setRoleId(request.getRoleId());
            user.setIsDeleted("N");
            user.setStatus(StatusEnum.ACTIVE.getDescription());
            user.setCreatedDate(new Date());
            userRepository.save(user);

            response = response.success();
            response.setData(new UserAssembler().toDTO(user));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(response.internalServerError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
