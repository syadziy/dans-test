/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.service;

import com.dans.test.dto.AuthRequestDTO;
import com.dans.test.dto.AuthResponseDTO;
import com.dans.test.entity.UserEntity;
import com.dans.test.enums.StatusEnum;
import com.dans.test.general.GeneralFunction;
import com.dans.test.repository.UserRepository;
import com.dans.test.response.ResponseDTO;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author arfinsyadziy
 */
@Service
public class AuthService {

    @Autowired
    private GeneralFunction gFunction;

    @Autowired
    private UserRepository userRepository;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public ResponseEntity<?> login(ResponseDTO response, AuthRequestDTO request) throws NoSuchAlgorithmException {
        try {
            UserEntity user = userRepository.findUserByUsername(request.getUsername());

            if (user == null) {
                return new ResponseEntity<>(response.notFound(request.getUsername()), HttpStatus.OK);
            }

            if (!user.getPassword().equals(gFunction.generatePassword(request.getPassword(), "dans_test"))) {
                user.setCounterWrongPassword(user.getCounterWrongPassword() == null ? 1 : (user.getCounterWrongPassword() + 1));
                return new ResponseEntity<>(response.wrongPassword(), HttpStatus.OK);
            }

            if (!user.getStatus().equals(StatusEnum.ACTIVE.getDescription())) {
                return new ResponseEntity<>(response.notActive(request.getUsername()), HttpStatus.OK);
            }

            if (user.getIsDeleted().equals("Y")) {
                return new ResponseEntity<>(response.isDeleted(request.getUsername()), HttpStatus.OK);
            }

            Calendar cal = Calendar.getInstance();
            user.setLastLogin(cal.getTime());

            String token = gFunction.generateToken(user.getPassword(), "secret_key", "dans_test");
            user.setToken(token);

            cal.add(Calendar.HOUR, 1);
            formatter = new SimpleDateFormat("yyyy-MM-dd HH");

            user.setTokenExpired(formatter.format(cal.getTime()));
            userRepository.save(user);

            AuthResponseDTO loginResponse = new AuthResponseDTO();
            loginResponse.setTokenType("Bearer");
            loginResponse.setAccessToken(token);

            response = response.success();
            response.setData(loginResponse);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(response.internalServerError(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
