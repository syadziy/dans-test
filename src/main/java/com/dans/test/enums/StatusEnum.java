/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.enums;

/**
 *
 * @author arfinsyadziy
 */
public enum StatusEnum {
    ACTIVE(1),
    INACTIVE(0);
    
    private final int description;

    StatusEnum(int description) {
        this.description = description;
    }

    public int getDescription() {
        return description;
    }
}
