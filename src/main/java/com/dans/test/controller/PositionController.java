/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.controller;

import com.dans.test.response.ResponseDTO;
import com.dans.test.service.PositionService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author arfinsyadziy
 */
@Controller
@RequestMapping("/api/v1/masterdata/position/")
public class PositionController {
    
    @Autowired
    private PositionService positionService;
    
    @GetMapping("get")
    public ResponseEntity<?> get() {
        ResponseDTO response = new ResponseDTO();
        return positionService.get(response);
    }
    
    @GetMapping("get/{id}")
    public ResponseEntity<?> get(@PathVariable String id) {
        ResponseDTO response = new ResponseDTO();
        return positionService.getById(response, id);
    }
    
    @GetMapping("download")
    public void download(HttpServletResponse response) throws IOException {
        try {
            positionService.download(response);
        } catch (IOException error) {
            error.printStackTrace();
        }
    }
    
}
