/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.controller;

import com.dans.test.dto.UserRequestDTO;
import com.dans.test.general.GeneralFunction;
import com.dans.test.response.ResponseDTO;
import com.dans.test.service.UserService;
import java.security.NoSuchAlgorithmException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author arfinsyadziy
 */
@Controller
@RequestMapping("/api/v1/masterdata/user/")
public class UserController {
    
    @Autowired
    private GeneralFunction gFunction;
    
    @Autowired
    private UserService userService;
    
    @PostMapping("create")
    public ResponseEntity<?> create(@Valid @RequestBody UserRequestDTO request, Errors errors, BindingResult result) throws NoSuchAlgorithmException {
        ResponseDTO response = new ResponseDTO();
        if (gFunction.validation(result) != null) {
            response = response.badRequest();
            response.setMessage(gFunction.validation(result));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        
        return userService.create(response, request);
    }
    
}
