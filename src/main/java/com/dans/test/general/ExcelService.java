/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.general;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author arfinsyadziy
 */
@Service
public class ExcelService {
    
    private static int i = 0;
    private static String directoryPath;

    @Value("${directory.path}")
    public void setDirectoryPath(String value) {
        this.directoryPath = value;
    }
    
    public static void header(XSSFSheet sheet, XSSFWorkbook workbook) {
        String today = new SimpleDateFormat("dd MMM yyyy HH:mm").format(new Date());

        XSSFCellStyle background = workbook.createCellStyle();
        XSSFCellStyle boldStyle = workbook.createCellStyle();
        XSSFCellStyle italicStyle = workbook.createCellStyle();
        sheet.setDisplayGridlines(false);

        XSSFFont defaultFont = workbook.createFont();
        defaultFont.setFontHeightInPoints((short) 10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);
        defaultFont.setItalic(false);

        XSSFFont bold = workbook.createFont();
        bold.setFontHeightInPoints((short) 10);
        bold.setFontName("Arial");
        bold.setColor(IndexedColors.BLACK.getIndex());
        bold.setBold(true);
        bold.setItalic(false);
        bold.setFontHeightInPoints((short) 14);

        XSSFFont boldWhite = workbook.createFont();
        boldWhite.setFontHeightInPoints((short) 10);
        boldWhite.setFontName("Arial");
        boldWhite.setColor(IndexedColors.WHITE.getIndex());
        boldWhite.setBold(true);
        boldWhite.setItalic(false);

        XSSFFont italic = workbook.createFont();
        italic.setFontHeightInPoints((short) 10);
        italic.setFontName("Arial");
        italic.setColor(IndexedColors.BLACK.getIndex());
        italic.setBold(false);
        italic.setItalic(true);

        boldStyle.setFont(bold);
        Row headerRow = sheet.createRow(0);
        headerRow.setHeightInPoints((short) 20);
        Cell headerCell = headerRow.createCell(0);
        headerCell.setCellStyle(boldStyle);
        headerCell.setCellValue("DOWNLOAD DATA");

        Row secondRow = sheet.createRow(1);
        secondRow.createCell(0).setCellValue("POSITION");

        italicStyle.setFont(italic);
        Row thirdRow = sheet.createRow(2);
        Cell thirdCell = thirdRow.createCell(0);
        thirdCell.setCellStyle(italicStyle);
        thirdCell.setCellValue("Data per " + today);

        background.setFont(boldWhite);
        background.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        background.setFillPattern(CellStyle.SOLID_FOREGROUND);
        background.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        background.setBorderTop(HSSFCellStyle.BORDER_THIN);
        background.setBorderRight(HSSFCellStyle.BORDER_THIN);
        background.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        Row lastRow = sheet.createRow(3);

        Cell lastCell0 = lastRow.createCell(0);
        lastCell0.setCellStyle(background);
        lastCell0.setCellValue("ID");

        Cell lastCell1 = lastRow.createCell(1);
        lastCell1.setCellStyle(background);
        lastCell1.setCellValue("Type");

        Cell lastCell2 = lastRow.createCell(2);
        lastCell2.setCellStyle(background);
        lastCell2.setCellValue("URL");

        Cell lastCell3 = lastRow.createCell(3);
        lastCell3.setCellStyle(background);
        lastCell3.setCellValue("Created At");

        Cell lastCell4 = lastRow.createCell(4);
        lastCell4.setCellStyle(background);
        lastCell4.setCellValue("Company");

        Cell lastCell5 = lastRow.createCell(5);
        lastCell5.setCellStyle(background);
        lastCell5.setCellValue("Company URL");

        Cell lastCell6 = lastRow.createCell(6);
        lastCell6.setCellStyle(background);
        lastCell6.setCellValue("Location");

        Cell lastCell7 = lastRow.createCell(7);
        lastCell7.setCellStyle(background);
        lastCell7.setCellValue("Title");

        Cell lastCell8 = lastRow.createCell(8);
        lastCell8.setCellStyle(background);
        lastCell8.setCellValue("Description");

        Cell lastCell9 = lastRow.createCell(9);
        lastCell9.setCellStyle(background);
        lastCell9.setCellValue("How To Apply");

        Cell lastCell10 = lastRow.createCell(10);
        lastCell10.setCellStyle(background);
        lastCell10.setCellValue("Company Logo");
    }
    
    public void generateExcel(HttpServletResponse response, Workbook workbook, String fileName) throws FileNotFoundException, IOException {
        try (FileOutputStream outputStream = new FileOutputStream(directoryPath + fileName)) {
            workbook.write(outputStream);
        } finally {
            workbook.close();
        }

        String contentType = "application/octet-stream";
        byte[] file = getFileOnServer(fileName);

        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        response.setHeader("charset", "iso-8859-1");
        response.setContentType(contentType);
        response.setContentLength(file.length);
        response.setStatus(HttpServletResponse.SC_OK);

        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            outputStream.write(file, 0, file.length);
            outputStream.flush();
            outputStream.close();
            response.flushBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] getFileOnServer(String fileName) throws FileNotFoundException, IOException {
        final String path = directoryPath;
        FileInputStream input = new FileInputStream(path + fileName);

        return IOUtils.toByteArray(input);
    }
    
}
