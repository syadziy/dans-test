/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.general;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 *
 * @author arfinsyadziy
 */
@Service
public class GeneralFunction {

    public String validation(BindingResult result) {
        try {
            Class<?> aClass = result.getTarget().getClass();
            for (FieldError fe : result.getFieldErrors()) {
                Field f = aClass.getDeclaredField(fe.getField());
                JsonProperty annotation = f.getDeclaredAnnotation(JsonProperty.class);
                String fieldName = (annotation == null) ? fe.getField() : annotation.value();
                fieldName = fieldName.replaceAll(
                        String.format("%s|%s|%s",
                                "(?<=[A-Z])(?=[A-Z][a-z])",
                                "(?<=[^A-Z])(?=[A-Z])",
                                "(?<=[A-Za-z])(?=[^A-Za-z])"
                        ),
                        " "
                );
                
                return "Field " + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1) + " Cannot be Empty";
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return "Error Field Exception: " + e.getMessage();
        }

        return null;
    }
    
    public String generatePassword(String password, String salt) throws NoSuchAlgorithmException {
        String passwordEncripted;

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        passwordEncripted = sb.toString();

        return passwordEncripted;
    }
    
    public String generateToken(String password, String secretKey, String salt) throws NoSuchAlgorithmException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();
        String today = dateFormat.format(date);

        String tokenEncripted;
        salt = salt + today + secretKey;

        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] bytes = md.digest(password.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        tokenEncripted = sb.toString();
        return tokenEncripted;
    }

}
