/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.response;

import java.io.Serializable;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author arfinsyadziy
 */
@Data
@Getter
@Setter
public class ResponseDTO implements Serializable {

    private int status;
    private String message;
    private Object data;

    public ResponseDTO() {
    }

    public ResponseDTO(int status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ResponseDTO success() {
        return new ResponseDTO(200, "Success", null);
    }

    public ResponseDTO alreadyExist(String val) {
        return new ResponseDTO(200, val + " Already Exist", null);
    }

    public ResponseDTO wrongPassword() {
        return new ResponseDTO(200, "Wrong Password", null);
    }

    public ResponseDTO notActive(String val) {
        return new ResponseDTO(200, val + " Not Active", null);
    }

    public ResponseDTO isDeleted(String val) {
        return new ResponseDTO(200, val + " Already Deleted", null);
    }

    public ResponseDTO badRequest() {
        return new ResponseDTO(400, "Bad Request", null);
    }

    public ResponseDTO forbiddenError() {
        return new ResponseDTO(403, "You Don't Have Authorization", null);
    }

    public ResponseDTO notFound(String val) {
        return new ResponseDTO(404, val + " Not Found", null);
    }

    public ResponseDTO internalServerError() {
        return new ResponseDTO(500, "Internal Server Error", null);
    }

    public ResponseDTO temporaryServiceUnavailable() {
        return new ResponseDTO(503, "Temporary Service Unavailable", null);
    }

    public ResponseDTO responseCodeNotFound() {
        return new ResponseDTO(0, "Response Code Not Found", null);
    }

}
