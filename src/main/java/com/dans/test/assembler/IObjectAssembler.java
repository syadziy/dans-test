/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.assembler;

/**
 *
 * @author arfinsyadziy
 */
public interface IObjectAssembler<X, Y> {
    Y toDTO(X domainObject);
    X toDomain(Y dtoObject);
}
