/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.assembler;

import com.dans.test.dto.UserResponseDTO;
import com.dans.test.entity.UserEntity;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author arfinsyadziy
 */
public class UserAssembler implements IObjectAssembler<UserEntity, UserResponseDTO> {

    @Override
    public UserResponseDTO toDTO(UserEntity domainObject) {
        UserResponseDTO dto = new UserResponseDTO();
        dto.setId(domainObject.getId());
        dto.setFirstName(domainObject.getFirstName());
        dto.setLastName(domainObject.getLastName());
        dto.setFullName(domainObject.getFullName());
        dto.setEmail(domainObject.getEmail());
        dto.setUsername(domainObject.getUsername());
        dto.setTokenExpired(domainObject.getTokenExpired());
        dto.setIsDeleted(domainObject.getIsDeleted());
        dto.setReasonBlocked(domainObject.getReasonBlocked());
        dto.setIpAddress(domainObject.getIpAddress());
        dto.setUserAgent(domainObject.getUserAgent());
        dto.setUserUuid(domainObject.getUserUuid());
        dto.setRoleId(domainObject.getRoleId());
        dto.setStatus(domainObject.getStatus());
        dto.setLastLogin(domainObject.getLastLogin());
        dto.setLastChangePassword(domainObject.getLastChangePassword());
        dto.setCreatedDate(domainObject.getCreatedDate());
        dto.setLastUpdate(domainObject.getLastUpdate());
        return dto;
    }

    @Override
    public UserEntity toDomain(UserResponseDTO dtoObject) {
        UserEntity entity = new UserEntity();
        entity.setId(dtoObject.getId());
        entity.setFirstName(dtoObject.getFirstName());
        entity.setLastName(dtoObject.getLastName());
        entity.setFullName(dtoObject.getFullName());
        entity.setEmail(dtoObject.getEmail());
        entity.setUsername(dtoObject.getUsername());
        entity.setTokenExpired(dtoObject.getTokenExpired());
        entity.setIsDeleted(dtoObject.getIsDeleted());
        entity.setReasonBlocked(dtoObject.getReasonBlocked());
        entity.setIpAddress(dtoObject.getIpAddress());
        entity.setUserAgent(dtoObject.getUserAgent());
        entity.setUserUuid(dtoObject.getUserUuid());
        entity.setRoleId(dtoObject.getRoleId());
        entity.setStatus(dtoObject.getStatus());
        entity.setLastLogin(dtoObject.getLastLogin());
        entity.setLastChangePassword(dtoObject.getLastChangePassword());
        entity.setCreatedDate(dtoObject.getCreatedDate());
        entity.setLastUpdate(dtoObject.getLastUpdate());
        return entity;
    }

    public List<UserResponseDTO> toDTOs(List<UserEntity> arg0) {
        List<UserResponseDTO> res = new ArrayList<>();
        arg0.stream().forEach((o) -> {
            res.add(toDTO(o));
        });
        return res;
    }

    public Set<UserEntity> toDomains(List<UserResponseDTO> arg0) {
        Set<UserEntity> res = new HashSet<>();
        arg0.stream().forEach((o) -> {
            res.add(toDomain(o));
        });
        return res;
    }

}
