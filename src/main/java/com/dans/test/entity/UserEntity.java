/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

/**
 *
 * @author arfinsyadziy
 */
@Data
@Getter
@Setter
@Entity
@Table(name = "mst_user")
public class UserEntity {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mst_user_gen")
    @SequenceGenerator(name = "mst_user_gen", sequenceName = "mst_user_id_seq", allocationSize = 1)
    @Column(name = "id", length = 11, nullable = false)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    @Type(type = "text")
    private String password;

    @Column(name = "token")
    @Type(type = "text")
    private String token;
    
    @Column(name = "token_expired")
    private String tokenExpired;

    @Column(name = "is_deleted")
    private String isDeleted;

    @Column(name = "reason_blocked")
    private String reasonBlocked;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "verify_code")
    private String verifyCode;

    @Column(name = "user_uuid")
    private String userUuid;

    @Column(name = "role_id")
    private Integer roleId;

    @Column(name = "status")
    private Integer status;

    @Column(name = "counter_wrong_password")
    private Integer counterWrongPassword;
    
    @Column(name = "last_login")
    private Date lastLogin;
    
    @Column(name = "last_change_password")
    private Date lastChangePassword;
    
    @Column(name = "created_date")
    private Date createdDate;
    
    @Column(name = "last_update")
    private Date lastUpdate;
    
}
