/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dans.test.repository;

import com.dans.test.entity.UserEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author arfinsyadziy
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    @Override
    List<UserEntity> findAll();
    List<UserEntity> findUserByStatus(Integer status);
    UserEntity findUserById(Integer id);
    UserEntity findUserByUserUuid(String userUuid);
    UserEntity findUserByUsername(String username);
    UserEntity findUserByEmail(String email);
    @Query(value = "select * from mst_user where id != :id and username = :username and is_deleted = 'N' and status = 1", nativeQuery = true)
    UserEntity findUserByNotIdAndUsername(@Param("id") Integer id, @Param("username") String username);
}
